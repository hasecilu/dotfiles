# PartKeepr

## Backup data

Commands to backup the volumes using a `busybox` container. Consider containers named: `partkeepr-cronjob-1`, `partkeepr-database-1` and `partkeepr-partkeepr-1`.
- If you find any problem with busybox you can substitute `busybox` for `ubuntu` on the commands.
- You can omit the `v` flag on `tar` to avoid verbosity.

1. Move to the folder where you want to save your backups: `cd ~/backups/partkeepr`.
2. Stop the containers: `docker stop partkeepr-partkeepr-1 partkeepr-database-1 partkeepr-cronjob-1`
3. Execute these commands to create compressed tarballs.
```shell
docker run --rm --volumes-from partkeepr-database-1 -v $(pwd):/backup \
    busybox tar cvzf /backup/backup_database_$(date +"%Y-%m-%d_%H-%M").tar /var/lib/mysql
docker run --rm --volumes-from partkeepr-partkeepr-1 -v $(pwd):/backup \
    busybox tar cvzf /backup/backup_partkeepr_$(date +"%Y-%m-%d_%H-%M").tar /var/www/html/app/config /var/www/html/data /var/www/html/web
```
4. Copy the resultant tarballs using `scp` or `rsync` to your hard drive or cloud to backup them.

Example function to backup partkeepr
```shell
pk_backup() {
	cd $HOME/disk/Backups/server/partkeepr || exit
	figlet Making backup of PartKeepr database | lolcat
	docker run --rm --volumes-from partkeepr-database-1 -v $(pwd):/backup busybox tar cvzf \
		/backup/backup_database_$(date +"%Y-%m-%d_%H-%M").tar /var/lib/mysql
	figlet Making backup of PartKeepr app | lolcat
	docker run --rm --volumes-from partkeepr-partkeepr-1 -v $(pwd):/backup busybox tar cvzf \
		/backup/backup_partkeepr_$(date +"%Y-%m-%d_%H-%M").tar /var/www/html/app/config \
		/var/www/html/data /var/www/html/web
	echo && ls -l && cd -
}
```
## Restore data

After creating a **new** partkeepr docker stack follow next steps to restore your data using a `busybox` container.
- If you find any problem with busybox you can substitute `busybox sh` for `ubuntu bash` on the commands.
- You can omit the `v` flag on `tar` to avoid verbosity.

1. **Important!** Move to the folder where you have your backups: `cd ~/backups/partkeepr`. Needed to mount the tarball in the container.
2. **Important!** Stop the containers: `docker stop partkeepr-partkeepr-1 partkeepr-database-1 partkeepr-cronjob-1`. It doesn't work when the containers are runnning.
3. Execute these commands to extract the compressed tarballs into their respective volumes.
```shell
docker run --rm --volumes-from partkeepr-database-1 -v $(pwd):/backup \
    busybox sh -c "cd / && tar xvf /backup/backup_database_2023-03-14_16-00.tar"
docker run --rm --volumes-from partkeepr-partkeepr-1 -v $(pwd):/backup \
    busybox sh -c "cd / && tar xvf /backup/backup_partkeepr_2023-03-14_16-00.tar"
```
4. Start again the containers: `docker start partkeepr-partkeepr-1 partkeepr-database-1 partkeepr-cronjob-1` and use your credentials to access the UI.

Example function to restore partkeepr
```shell
pk_restore() {
	cd $HOME/disk/Backups/server/partkeepr || exit
	# subdirectories are included in the search
	last_pkdb="$(find . -type f -name \*database*.tar -printf "%T@ %p\0" | awk 'BEGIN {RS="\0";} {if (NR==1){minmtime=$1; $1=""; lastmodified=$0;} else if (minmtime<$1){minmtime=$1; $1=""; lastmodified=$0;}} END{print substr(lastmodified,4)}')"
	last_pkpk="$(find . -type f -name \*partkeepr*.tar -printf "%T@ %p\0" | awk 'BEGIN {RS="\0";} {if (NR==1){minmtime=$1; $1=""; lastmodified=$0;} else if (minmtime<$1){minmtime=$1; $1=""; lastmodified=$0;}} END{print substr(lastmodified,4)}')"
	figlet Restoring PartKeepr database | lolcat
	echo -e "\nRestoring $(pwd)/$last_pkdb tarball\n" | lolcat
	docker run --rm --volumes-from partkeepr-database-1 -v $(pwd):/backup busybox sh -c "cd / && tar xvf /backup/$last_pkdb"
	figlet Restoring PartKeepr app | lolcat
	echo -e "\nRestoring $(pwd)/$last_pkpk tarball\n" | lolcat
	docker run --rm --volumes-from partkeepr-partkeepr-1 -v $(pwd):/backup busybox sh -c "cd / && tar xvf /backup/$last_pkpk"
}
```
