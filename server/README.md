# home-server

Personal home server to share files/media and use some server apps.

![Server homepage](../images/server.png)

## Installing Debian server

0. Always check hardware is okay.
1. Install Debian server from official ISO image.
2. Execute [post-install script][00].
3. Finish other settings manually.
    - Copy `SSH` keys
    - Restore backups of media files
    - Restore `samba` shares
    - Restore backups of `docker` volumes
    - Configure `fstab` file

        ```shell
        lsblk
        sudo blkid
        # UUID=012345b3-0123-4567-89ab-cdefecacafed /mnt/disk       ext4    defaults        0       0 >> /etc/fstab
        ```

    - Restore `vnstat` database

        ```shell
        sudo systemctl stop vnstat
        sudo install --owner vnstat --group vnstat --mode 0644 -v /backup/vnstat.db /var/lib/vnstat/vnstat.db
        sudo systemctl start vnstat
        ```

|                   Operation                   |    Time    |
| --------------------------------------------- | :--------: |
| Burn netinst Debian ISO (using `dd`) into USB | **1m35s**  |
| Install Debian (graphical interface)          | **18m30s** |
| Run [post-install script][00]                 | **22m30s** |
| Finish settings manually                      | **17m25s** |
| Total                                         | **60m00s** |

### Filesystem partitions

| File system | Mount Point |   Size   | Flags |   Drive   |
| ----------- | ----------- | -------- | :---: | :-------: |
| ext4        | /mnt/disk   | 915.8GiB |   -   | HDD (sda) |
| ext4        | /           | 45.5GiB  |   B   | HDD (sdb) |
| ext4        | /home       | 866.1GiB |   -   | HDD (sdb) |
| linuxswap   | -           | 4GiB     |   -   | HDD (sdb) |

## Natively installed services

- Jellyfin
- Pi-hole

### `systemd` commands

Some important commands using systemd.

- View all services

    ```shell
    systemctl list-unit-files --type=service
    ```

- Start a service

    ```shell
    sudo systemctl start sshd
    ```

- Stop a service

    ```shell
    sudo systemctl stop deluged
    ```

- Check status of a service

    ```shell
    systemctl status vnstatd
    ```

- Enable a service, the `--now` flag starts the service

    ```shell
    sudo systemctl enable [--now] smbd
    ```

- Disable a service, the `--now` flag stops the service

    ```shell
    sudo systemctl disable [--now] jellyfin
    ```

- Restart service, `reload` if preferred over `restart`, it won't stop the
process but it will reload config files

    ```shell
    sudo systemctl restart rsync
    ```

- Read logs from service

    ```shell
    sudo journalctl -u jellyfin.service
    ```

## Docker services

- Deluge
- Heimdall
- Inventree
- Radicale
- Speedtest Tracker
- TriliumNext
- WatchYourLAN

### Docker commands

Some important Docker commands.

- View available images

    ```shell
    docker images
    ```

- Update a single image, `publisher/package_name:version`

    ```shell
    docker pull mattermost/focalboard:latest
    ```

- Update all images in a stack

    ```shell
    docker compose pull
    ```

- View running containers, using `-a` includes stopped containers

    ```shell
    docker ps [-a]
    ```

- Stop and remove container using id

    ```shell
    docker stop <container_id>
    docker rm <container_id>
    ```

- Enter into a running container filesystem

    ```shell
    docker exec -it <container-id> sh
    ```

- Create and start the containers (as a deamon) using the `docker-compose.yml`
file on current directory

    ```shell
    docker compose up -d
    ```

- Stop containers and removes containers, networks, volumes, and images created
by `up`, using `-v` flag can lead to data loss

    ```shell
    docker compose down
    ```

- View logs of a running container

    ```shell
    docker logs -f <container-id>
    ```

- Copy data from a container

    ```shell
    docker cp <container-id>:/path/to/data ./data
    ```

- Update all containers, not worth it because can break  services

    ```shell
    docker run --rm -v /var/run/docker.sock:/var/run/docker.sock containrrr/watchtower --run-once --debug
    ```

- Backup non-persistent volumes

    ```shell
    docker run --rm --volumes-from container-name -v $(pwd):/backup \
        busybox tar cvzf /backup/name_of_the_tarball_$(date +"%Y-%m-%d_%H-%M").tar /path1/to/data /path2/to/data
    ```

- Restore non-persistent volumes

    ```shell
    docker run --rm --volumes-from container-name -v $(pwd):/backup \
        busybox sh -c "cd / && tar xvf /backup/name_of_the_tarball__2023-03-14_16-00.tar"
    ```

- Remove unused data

    ```shell
    docker container prune -f
    docker image prune -a -f
    docker volume prune -f
    docker network prune -f
    docker builder prune -f
    ```

- List dangling containers

    ```shell
    docker volume ls -qf dangling=true
    ```

### Read more

- [lazydocker][01]

[00]: https://gitlab.com/hasecilu/dotfiles/-/blob/main/post-install-scripts/debian-server.sh
[01]: https://github.com/jesseduffield/lazydocker
