# dconf

As they are just text files they can be manually edited.

## Backup settings


```shell
dconf dump /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/ > custom-keybindings.dconf
dconf dump /org/gnome/shell/extensions/ > gnome-extension-settings.dconf
dconf dump /com/gexperts/Tilix/ > tilix-profile.dconf
```

## Restore settings


```shell
dconf load /org/gnome/settings-daemon/plugins/media-keys/custom-keybindings/ < custom-keybindings.dconf
dconf load /org/gnome/shell/extensions/ < gnome-extension-settings.dconf
dconf load /com/gexperts/Tilix/ < tilix-profile.dconf
```

