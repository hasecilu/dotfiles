#!/bin/bash

# ---------------------------------------------------------------------------
#
# Script to automate (as much as possible) setup of fresh Debian server.
#
# Created by hasecilu
#
# License: WTFPL	https://en.wikipedia.org/wiki/WTFPL
#
# ---------------------------------------------------------------------------

NAME="Debian server post-install script"
VERSION="0.6"

# ---------------------------------------------------------------------------
# Functions

function main() {
	splash "$NAME"
	echo_caption "Welcome to the Debian server post install script V$VERSION"
	echo_warning "\nStill is alpha version"

	install_sudo

	# Update
	echo_p "Updating the system"
	sudo apt update && apt list --upgradable && sudo apt upgrade -y

	# Install programs from the debian repos
	echo_p "Installing programs from the debian repos"
	sudo apt install -y cifs-utils curl figlet gcc g++ git htop lolcat make \
		podman{,-compose} python3-{pip,venv} rsync samba smbclient sl tmux tree vnstat

	# Install other programs
	echo_p "Downloading deb files from third party repositories"

	## Install programs using the deb-get package
	echo_p "\tInstalling programs using the deb-get package"
	curl -sL https://raw.githubusercontent.com/wimpysworld/deb-get/main/deb-get | sudo -E bash -s install deb-get

	deb-get install du-dust duf fastfetch fd rclone

	# Install programs from external sources
	echo_p "Installing programs from external sources"

	## Docker
	echo -e "\n\n"
	read -p "Do you want to install docker & compose plugin? [y/N] : " -n 1 -r
	[[ $REPLY =~ ^[Yy]$ ]] && docker-compose || echo -e "\nDocker is not being installed"

	## netdata
	#wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kicdkstart.sh && sh /tmp/netdata-kickstart.sh

	## Jellyfin
	echo -e "\n\n"
	read -p "Do you want to install Jellyfin? [y/N] : " -n 1 -r
	[[ $REPLY =~ ^[Yy]$ ]] && curl https://repo.jellyfin.org/install-debuntu.sh | sudo bash || echo -e "\nJellyfin is not being installed"

	## Pi-hole
	echo -e "\n\n"
	read -p "Do you want to install pihole? [y/N] : " -n 1 -r
	[[ $REPLY =~ ^[Yy]$ ]] && pihole || echo -e "\nPihole is not being installed"

	## OpenMediaVault
	echo -e "\n\n"
	read -p "Do you want to install OpenMediaVault? [y/N] : " -n 1 -r
	[[ $REPLY =~ ^[Yy]$ ]] && omv || echo -e "\nOpenMediaVault is not being installed"

	# Copy configuration dotfiles
	echo_p "Copying configuration files"
	cp -v home/.aliases ~/
	mkdir -pv ~/.config/nvim/
	cp -v home/.config/nvim/init.lua ~/.config/nvim/
	cp -rv home/.config/tmux/ ~/.config/
	cp -rv home/.config/htop/ ~/.config/

	# Add a tone to GRUB
	echo_p "Adding GRUB tone"
	sudo sed -i 's/#GRUB_INIT_TUNE="480 440 1"/GRUB_INIT_TUNE="1750 523 1 392 1 523 1 659 1 784 1 1047 1 784 1 415 1 523 1 622 1 831 1 622 1 831 1 1046 1 1244 1 1661 1 1244 1 466 1 587 1 698 1 932 1 1195 1 1397 1 1865 1 1397 1"/g' /etc/default/grub
	sudo update-grub

	# Install oh-my-bash
	echo_p "Installing oh-my-bash"
	bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"

	{
		echo -e "\nsource \$HOME/.aliases"
		echo -e "\necho && uptime | lolcat"
		echo "figlet home-server | lolcat"
		echo -e "neofetch\n"
	} >>~/.bashrc

	echo_bold "\n\n\nDon't forget to configure the network interface and ssh server."

	echo_primary "\n終わりました\n\n"

	neofetch
}

function help() {
	echo
	echo_caption "-- $NAME Version: $VERSION --"
	echo
	echo_primary "Description :"
	echo_secondary "\t\tThis script helps you to setup a fresh Debian server installation. Manual intervention could be needed.\n"
	echo_primary "Usage :"
	echo_secondary "\t\tbash ./post-install-scripts/debian-server.sh [-h/-d/-o]\n"
	echo
	echo "-------------"
	echo_primary "Arguments :"
	echo_secondary "\t\t-h/--help         "
	echo_info "help command"
	echo_secondary "\t\t-d/--docker       "
	echo_info "install Docker"
	echo_secondary "\t\t-o/--omv          "
	echo_info "install OpemMediaVault"
	echo_secondary "\t\t-p/--pihole       "
	echo_info "install Pi-hole"
}

function install_sudo() {
	# install sudo and add user to sudo group
	if [ ! -x "$(command -v sudo)" ]; then
		echo_bold "sudo command is not installed\n"
		echo_primary "Insert your root password."
		su - root -c "apt update && apt install -y sudo && usermod -aG sudo $USER"
		echo_primary "To apply changes on $USER user you need to exit (close session), \
			re-login as $USER user and execute again this bash script. \
			With permissions on sudo group won't be problems."
		exit 0
	else
		echo_bold "sudo is already installed.\n"
	fi
}

function pihole() {
	echo
	echo_p "\tpihole"
	curl -sSL https://install.pi-hole.net | bash
}

function docker-compose() {
	echo
	echo_p "\tdocker cli + docker compose plugin"
	# https://docs.docker.com/engine/install/debian/#install-using-the-repository
	# 1. Install packages to allow apt to use a repository over HTTPS
	sudo apt-get update
	sudo apt-get install -y ca-certificates curl
	# 2. Add Docker’s official GPG key
	sudo install -m 0755 -d /etc/apt/keyrings
	sudo curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc
	sudo chmod a+r /etc/apt/keyrings/docker.asc
	# 3. Command to set up the repository
	echo \
		"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" |
		sudo tee /etc/apt/sources.list.d/docker.list >/dev/null
	# 4. Update repositories
	sudo apt-get update
	# 5. Install Docker Engine, containerd, and Docker Compose.
	sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
	# 6. Verify that the Docker Engine installation is successful by running the hello-world image:
	sudo docker run hello-world
	# 7. Add user to docker group
	if id -nG "$USER" | grep -qw docker; then
		echo_primary "$USER  already belongs to docker group\n\n"
	else
		sudo usermod -aG docker "$USER"
		newgrp docker
	fi

	echo_primary "Install the other Docker containers using Portainer\n\n"
}

# ---------------------------------------------------------------------------
# Main code

if [ "${PWD##*/}" != "dotfiles" ]; then
	function echo_error() { echo -ne "\033[0;1;31merror:\033[0;31m\t${*}\033[0m\n"; }
	echo_error "The working directory must be 'dotfiles' (the cloned repository root) while executing this script."
	exit 1
else
	source ./post-install-scripts/lib.sh
	if [ $# -gt 1 ]; then
		help
	elif [ $# -eq 1 ]; then
		if [[ $1 == "-h" || $1 == "--help" ]]; then
			help
		elif [[ $1 == "-d" || $1 == "--docker" ]]; then
			docker-compose
		elif [[ $1 == "-p" || $1 == "--pihole" ]]; then
			pihole
		fi
	elif [ $# -eq 0 ]; then
		main
	fi
fi
