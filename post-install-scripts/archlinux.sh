#!/usr/bin/env bash

# ------------------------------------------------------------------------------
#
# Script to automate (as much as possible) setup of new Arch Linux install
#
# This script assumes a minimal working system with all basic services running,
# this script only install user applications, and some config files.
#
# Created by hasecilu
#
# License: WTFPL	(https://es.wikipedia.org/wiki/WTFPL)
#
# ------------------------------------------------------------------------------

NAME="Arch Linux post-install script"
VERSION="1.1.0"

# ------------------------------------------------------------------------------
# Functions

function main() {
	splash "$NAME"
	echo_caption "Welcome to the $NAME V$VERSION"

	if [ ! -x "$(command -v paru)" ]; then
		git clone https://aur.archlinux.org/paru-bin.git
		cd paru-bin || return
		makepkg -si
	fi

	echo_p "Updating the system"
	paru --noconfirm -Syyu

	# Either needed to compile some programs/dependencies or for development
	echo_p "Programming languages toolchain/package managers"
	pS clang go lua npm rust

	echo_p "Development programs"
	pS base-devel cmake curl entr gdb git{,-delta,-extras} hyperfine indent \
		jq lazygit neovim pre-commit python-{pip,pynvim,setuptools,virtualenv} \
		reflector shellcheck stow valgrind wget # gnome-boxes meld

	echo_p "Terminal programs"
	pS alacritty ascii{nema,quarium,-image-converter-bin} bat btop cmatrix c-lolcat duf \
		dust eza fastfetch fd ffmpeg figlet fzf htop immich-go \
		perl{-image-exiftool,rename} onefetch procs rclone ripgrep \
		rsync sl scour scrot sqlite tldr tmux vnstat xdg-ninja xsel \
		yt-dlp zoxide

	# First restore database
	sudo systemctl enable --now vnstat

	echo_p "Games"
	pS supertux # 0ad steam

	echo_p "Edition & multimedia programs"
	pS breeze inkscape kdenlive loupe mpv{,-mpris,-thumbfast-git} \
		obs-studio snapshot # webcamoid

	/bin/bash -c "$(curl -fsSL \
		https://raw.githubusercontent.com/tomasklaen/uosc/HEAD/installers/unix.sh)"

	echo_p "Internet programs"
	pS firefox librewolf-bin # deluge-gtk

	echo_p "Communication"
	pS discord {element,telegram}-desktop tutanota-desktop-bin \
		thunderbird # signal-desktop

	echo_p "Office programs"
	pS evince libreoffice-fresh{,-es} # simple-scan hplip

	echo_p "Privacy programs"
	pS hblock keepassxc kloak

	echo_p "GUI applications"
	pS appimagelauncher czkawka-gui-bin dialect keepassxc mousai nautilus poedit \
		qt{5,6}ct spotify tdrop tor-browser-bin tuba

	echo_p "Computing"
	pS jupyterlab # octave

	echo_p "TeX"
	pS texstudio texlive-latexrecommended

	echo_p "CAD programs"
	pS kicad{,-library,-library-3d} openscad solvespace \
		openfoam-org paraview # freecad

	echo_p "Hardware-development related tools"
	pS openhantek6022-git prusa-slicer pulseview stl-thumb \
		platformio-core{,-udev} # universal-gcode-sender-bin

	echo_p "uC tools: compilers, SDKs, etc."
	pS avr{dude,-gcc,-lib} sdcc

	echo_p "Nightly programs from the AUR for testing"
	pS freecad-weekly-appimage # kicad{,-library,-library-3d}-nightly

	echo_p "Themes"
	pS sddm-sugar-dark

	echo_p "Fonts"
	pS 3270-fonts adobe-source-han-{sans,serif}-jp-fonts figlet-fonts{,-extra} \
		noto-fonts-{cjk,emoji} otf-cascadia-code ttc-iosevka \
		ttf-{agave,droid,fira-code,hack,inconsolata,envy-code-r,victor-mono,vlgothic}

	echo_p "Support for Japanese input"
	pS fcitx5{,-mozc}

	echo_p "OCR"
	pS tesseract{,-data-{eng,jpn,spa}} gimagereader-gtk

	echo_p "Drivers"
	pS brother-hl2135w

	echo_p "For GNOME"
	pS dconf-editor extension-manager gnome-shell-pomodoro \
		gnome-tweaks guake lollypop tilix ulauncher wmctrl

	# Install flatpak
	paru --needed --noconfirm -S flatpak
	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

	# Install flatpak apps
	echo_p "Installing programs from flathub"
	flatpak install -y --user https://flathub.org/beta-repo/appstream/org.gimp.GIMP.flatpakref
	flatpak install -y flathub org.gtk.Gtk3theme.Adwaita-dark

	# Install some other programs with custom comands
	echo_p "Installing programs from third party repositories"

	zsh
	lazyvim

	# Symlink configuration dotfiles
	echo_p "Simulation before symlinking all configuration files"
	stow --adopt --simulate --verbose=3 --target="$HOME" --no-folding home
	read -p "Do you want to symlink the config files? [y/N] : " -n 1 -r
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		echo
		stow --adopt --target="$HOME" --no-folding home
	fi

	bash end.sh
}

function help() {
	echo
	echo_caption "-- $NAME Version: $VERSION --"
	echo
	echo_primary "Description :"
	echo_secondary "\t\tThis script helps you to setup a fresh ArcoLinux installation . Manual intervention could be needed.\n"
	echo_primary "Usage :"
	echo_secondary "\t\tbash ./post-install-scripts/arcolinux.sh [-h/-f/-g/-z]\n"
	echo
	echo "-------------"
	echo_primary "Arguments :"
	echo_secondary "-h/--help               "
	echo_info "help command"
	echo_secondary "-g/--gnome-extensions   "
	echo_info "install gnome-extensions"
	echo_secondary "-l/--lazyvim            "
	echo_info "install LazyVim"
	echo_secondary "-z/--zsh                "
	echo_info "install zsh and some plugins"
}

function zsh() {
	read -p "Do you want to install zsh and some plugins? [y/N] : " -n 1 -r
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		echo
		sudo pacman --needed --noconfirm -S zsh
		# Install oh-my-zsh
		echo_p "Installing oh-my-zsh"
		ZSH="$HOME"/.local/share/oh-my-zsh sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

		# Install power-level-10k
		echo_p "Installing power-level-10k"
		git clone --depth=1 https://github.com/romkatv/powerlevel10k.git \
			"${ZSH_CUSTOM:-$HOME/.local/share/oh-my-zsh/custom}"/themes/powerlevel10k

		# Install zsh-syntax-highlighting
		echo_p "Installing zsh-syntax-highlighting"
		git clone https://github.com/zsh-users/zsh-syntax-highlighting.git \
			"${ZSH_CUSTOM:-$HOME/.local/share/oh-my-zsh/custom}"/plugins/zsh-syntax-highlighting

		# Install zsh-autosuggestions
		echo_p "Installing zsh-autosuggestions"
		git clone https://github.com/zsh-users/zsh-autosuggestions \
			"${ZSH_CUSTOM:-$HOME/.local/share/oh-my-zsh/custom}"/plugins/zsh-autosuggestions
	fi
}

function lazyvim() {
	read -p "Do you want to install LazyVim? [y/N] : " -n 1 -r
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		echo
		echo_p "Installing LazyVim"
		git clone https://github.com/LazyVim/starter "$HOME"/.config/nvim
		echo_p "Copying some extra files for LazyVim"
		cp -r ../assets/LazyVim/lua "$HOME"/.config/nvim
		# echo_p "Installing kickstart"
		# git clone https://github.com/nvim-lua/kickstart.nvim.git "$HOME"/.config/kickstart
		# echo_p "Installing AstroNvim"
		# git clone --depth 1 https://github.com/AstroNvim/AstroNvim "$HOME"/.config/AstroNvim
	fi
}

# ------------------------------------------------------------------------------
# Main code

if [ "${PWD##*/}" != "dotfiles" ]; then
	function echo_error() { echo -ne "\033[0;1;31merror:\033[0;31m\t${*}\033[0m\n"; }
	echo_error "The working directory basename must be 'dotfiles' 
	(the cloned repository root) while executing this script."
	exit 1
else
	source ./post-install-scripts/lib.sh
	if [ $# -gt 1 ]; then
		echo_error "Read help."
		echo_bold "Try bash ./post-install-scripts/arcolinux.sh --help"
		exit 1
	elif [ $# -eq 1 ]; then
		if [[ $1 == "-h" || $1 == "--help" ]]; then
			help
		elif [[ $1 == "-g" || $1 == "--gnome-extensions" ]]; then
			rm -f ./install-gnome-extensions.sh
			wget -N -q https://raw.githubusercontent.com/cyfrost/install-gnome-extensions/master/install-gnome-extensions.sh \
				-O ./install-gnome-extensions.sh
			chmod +x install-gnome-extensions.sh && ./install-gnome-extensions.sh -e --file gnome-extensions.txt
		elif [[ $1 == "-l" || $1 == "--lazyvim" ]]; then
			lazyvim
		elif [[ $1 == "-z" || $1 == "--zsh" ]]; then
			zsh
		fi
	elif [ $# -eq 0 ]; then
		main
	fi
fi
