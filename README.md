# Dotfiles

This repository contains my personal `dotfiles` for some software,
some scripts (check the [Snippets section][03] and a few guides.

## What are dotfiles?

Dotfiles are the customization files that are used to personalize your Linux or
other Unix-based system.

You can tell that a file is a dotfile because the name of the file will begin
with a period--a dot!  The period at the beginning of a filename or directory
name indicates that it is a hidden file or directory. This repository contains
my personal dotfiles.

They are stored here for convenience so that I may quickly access them on new
machines or new installs. Also, others may find some of my configurations
helpful in customizing their own dotfiles.

## My setup

A (in)comprehensible list of used software.

|                   Software                   |                                                    Description                                                    |           Source            |
| -------------------------------------------- | ----------------------------------------------------------------------------------------------------------------- | :-------------------------: |
| **Dark Matter \| CyberRE**                   | Dark Matter GRUB Theme \| Cyberpunk aesthetics GRUB theme                                                         | [repo][00] \| [website][02] |
| **GNU/Linux distribution**                   | Arch Linux                                                                                                        |      [Arch Linux][01]       |
| **zsh**                                      | A Unix shell that can be used as an interactive login shell and as a command interpreter for shell scripting.     |        [webpage][06]        |
| **oh my zsh**                                | A delightful community-driven (with 1800+ contributors) framework for managing your zsh configuration.            |         [repo][07]          |
| **Powerlevel10k**                            | A theme for Zsh. It emphasizes speed, flexibility and out-of-the-box experience.                                  |         [repo][08]          |
| **zsh-autosuggestions**                      | Fish-like fast/unobtrusive autosuggestions for zsh.                                                               |         [repo][09]          |
| **zsh-syntax-highlighting**                  | Fish shell-like syntax highlighting for Zsh.                                                                      |         [repo][10]          |
| **eza**                                      | A modern alternative to `ls`.                                                                                     |         [repo][11]          |
| **aliases**                                  | A bunch of personal aliases to speed up the workflow.                                                             |         [file][12]          |
| **Star Labs Cursor Theme Middle Finger Mod** | When you over a link, the cursor turns into a hand showing the middle finger instead of the default index finger. |        [website][14]        |

### Terminal look

Using Alacritty with `Symbols Nerd Font` as a fallback font.
Sometimes using `tmux` with custom color theme.

#### oh-my-zsh plugins

<p>
<details>
<summary>Click this to collapse/fold.</summary>

- catimg
- colored-man-pages
- copybuffer
- copyfile
- copypath
- dirhistory
- docker
- docker-compose
- extract
- git
- gitignore
- man
- pip
- python
- sudo
- tldr
- zsh-autosuggestions
- zsh-syntax-highlighting

</details>
</p>

![Terminal](images/Tilix.png)

#### FLOSS software that I use

<p>
<details>
<summary>Click this to collapse/fold.</summary>

- [Lollypop][19] is an excellent music player.
- [Variety][20] is an excellent wallpaper manager for GNU/Linux, you can even use ImageMagick to create filters.
- [Ulauncher][21] is a very handy launcher.
- [KiCad][22]
- [FreeCAD][23] weekly build
- [VNote][24]
- [Thunderbird][25]
- [TeXstudio][26]
- [Obsdian][28]
- [AppImageLauncher][29]
- [Boxes][30]

</details>
</p>

### Cursor

- [Star Labs Cursor Theme Middle Finger Mod][14]

### Menu icon on GNOME

I created a [bash script][44] to secure the change of the `Show Applications` icon to something of your suit.

You can download the `Tux` icon from @Ceccuz in this [website][33] <div>Icon made from <a href="http://www.onlinewebfonts.com/icon">Icon Fonts</a> is licensed by CC BY 3.0</div>

![Desktop Enviroment](images/GNOME.png)

## Post-install scripts

I have some post-install scripts: `Arch Linux` and `Debian`.
In the `Snippets` section I have some post-install scripts (just for reference): `Fedora`, `MX Linux` and `Ubuntu`.

These scripts install some applications that I use and make some configurations.

They are very useful when I need to reinstall the OS.

All my personal data is in a separated `/home` partition therefore I don't have to re-download it.

## Backup

I create my backups with GPG encryption. Check this [blog post][36] from putorius for more information.

**The time saved while downloading/uploading is worth the time used to compress using a .tar.xz file?** If no, use `.tar.gz` format.

You can use the `dtg()`, `dtx()`, `etg()`, `etx()` functions available on `home/.aliases` file.

### .tar.gz file

The time to create a .tar.xz file is less, requires less CPU computation, the resultant file is bigger.

#### Compress .tar.gz file

```shell
tar czvpf - ./folder_with_pseudo-important_files | gpg --symmetric --cipher-algo AES256 -o myarchive.gpg.tar.gz
```

#### Decompress .tar.gz file

```shell
gpg -d myarchive.gpg.tar.gz | tar xzvf -
```

### .tar.xz file

The time to create a .tar.xz file is much longer, requires more CPU computation, the resultant file is smaller.

#### Compress .tar.xz file

```shell
tar cJvpf - ./folder_with_pseudo-important_files | gpg --symmetric --cipher-algo AES256 -o myarchive.gpg.tar.xz
```

#### Decompress .tar.xz file

```shell
gpg -d myarchive.gpg.tar.xz | tar xJvf -
```

## Arch Linux

[Arch Linux][01] is a `GNU/Linux` distro to configure yourself.

### Installation

Moved to [Arch Linux installation guide](https://gitlab.com/hasecilu/dotfiles/-/snippets/2592951)

### Recover GRUB

Moved to [Recover GRUB menu](https://gitlab.com/hasecilu/dotfiles/-/snippets/2592950).

### Restore dotfiles as symlinks

`stow` is a symlink manager that lets you symlink files in bulk from a
directory to another preserving changes.

```shell
cd dotfiles
stow --adopt --simulate --verbose --target=$HOME home
# if good
stow --adopt --target=$HOME home
```

---

The end?

[00]: https://github.com/vandalsoul/darkmatter-grub2-theme
[01]: https://archlinux.org/
[02]: https://www.gnome-look.org/p/1420727/
[03]: https://gitlab.com/hasecilu/dotfiles/-/snippets
[06]: https://www.zsh.org/
[07]: https://github.com/ohmyzsh/ohmyzsh
[08]: https://github.com/romkatv/powerlevel10k
[09]: https://github.com/zsh-users/zsh-autosuggestions
[10]: https://github.com/zsh-users/zsh-syntax-highlighting
[11]: https://github.com/eza-community/eza
[12]: ./home/.config/zsh/,aliases
[14]: https://www.gnome-look.org/p/1292070/
[19]: https://gitlab.gnome.org/World/lollypop
[20]: https://github.com/varietywalls/variety
[21]: https://github.com/Ulauncher/Ulauncher
[22]: https://www.kicad.org/
[23]: https://www.freecadweb.org/
[24]: https://github.com/vnotex/vnote
[25]: https://www.thunderbird.net/en-US/
[26]: https://texstudio.org/
[28]: https://obsidian.md/
[29]: https://github.com/TheAssassin/AppImageLauncher
[30]: https://help.gnome.org/users/gnome-boxes/stable/
[33]: https://www.onlinewebfonts.com/icon/260481
[36]: https://www.putorius.net/how-to-create-enrcypted-password.html
[44]: https://gitlab.com/hasecilu/dotfiles/-/snippets/2542660
