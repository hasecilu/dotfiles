return {
  "nvim-tree/nvim-web-devicons",
  event = "VimEnter",
  opts = {
    strict = false, --true does not work
    --NOTE: Overrides must have "name" element assigned in order to get correct color.
    --"cterm_color" is not needed.
    --"name" must be different in order to accept the new color
    override_by_icon = {
      [""] = { icon = "" },
      [""] = { icon = "" },
    },
    override_by_filename = {
      ["containerfile"] = { icon = "", color = "#458ee6", cterm_color = "68" },
      ["data"] = { icon = "", color = "#71C613", name = "data" },
      ["init.lua"] = { icon = "", color = "#78B461", name = "NeovimInitLua" },
      ["input"] = { icon = "", color = "#82D724", name = "input" },
    },
    override_by_extension = {
      ["diff"] = { color = "#86AFF7", cterm_color = "69" },
      ["kube"] = { icon = "", color = "#458EE6", cterm_color = "68", name = "Kube" },
      ["log"] = { icon = "", color = "#FEFEFE", name = "log" },
      ["pod"] = { icon = "", color = "#458EE6", cterm_color = "68", name = "Pod" },
      ["po"] = { icon = "󰗊", color = "#2596BE" },
      ["pot"] = { icon = "󰗊", color = "#2596BE" },
      ["qm"] = { icon = "󰗊", color = "#2596BE", name = "QMCompiled" },
      ["sample"] = { icon = "󰷊", color = "#12D754", name = "Sample" },
      ["sh"] = { icon = "", color = "#12D754", name = "sh" },
      ["yaml"] = { icon = "", color = "#CC1018", name = "Yaml" },
      ["yml"] = { icon = "", color = "#CC1018", name = "Yml" },
    },
  },
}
