return {
  "folke/noice.nvim",
  requires = { "MunifTanjim/nui.nvim" },
  config = function()
    require("noice").setup({
      views = {
        confirm = {
          position = {
            row = "40%",
            col = "50%",
          },
        },
      },
    })
  end,
}
