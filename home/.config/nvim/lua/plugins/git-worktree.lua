return {
  "ThePrimeagen/git-worktree.nvim",
  keys = {
      -- stylua: ignore
    { "<leader>kt", function() require("telescope").extensions.git_worktree.git_worktrees() end, desc = "Swap worktree" },
      -- stylua: ignore
    { "<leader>kT", function() require("telescope").extensions.git_worktree.create_git_worktree() end, desc = "Create a new worktree" },
  },
}
