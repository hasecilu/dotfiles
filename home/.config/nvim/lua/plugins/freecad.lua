-- Map FreeCAD extensions to proper filetypes
vim.filetype.add({
  extension = {
    FCMacro = "python", --FreeCAD macro (Python code)
    FCScript = "python", --FreeCAD script (Python code)
    FCMat = "yaml", --FreeCAD material card
    FCParam = "xml", --FreeCAD parameter file
    fctb = "json", --CAM tool bit file (JSON format)
    fctl = "json", -- CAM tool library file (JSON format)
  },
})

-- Return the configuration for nvim-lspconfig
return {
  "neovim/nvim-lspconfig",
  opts = {
    servers = {
      pyright = {
        settings = {
          python = {
            analysis = {
              extraPaths = {
                "~/git/FreeCAD/freecad-stubs/freecad_stubs/",
                -- "~/.local/pipx/venvs/freecad-stubs/lib/python3.11/site-packages",
                "/usr/lib/freecad/lib/", -- to find FreeCAD and FreeCADGui
                "/usr/lib/freecad/Ext/", -- PySide alias for PySide6
                -- "/opt/FreeCAD_1_0_Qt6/lib/", -- to find FreeCAD and FreeCADGui
                -- "/opt/FreeCAD_1_0_Qt6/Ext/", -- PySide alias for PySide6
              },
              useLibraryCodeForTypes = true, -- infer type from code
            },
          },
        },
      },
    },
  },
}
