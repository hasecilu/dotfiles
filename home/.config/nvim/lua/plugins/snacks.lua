return {
  "folke/snacks.nvim",
  priority = 1000,
  lazy = false,
  opts = {
    styles = {
      lazygit = {
        relative = "editor",
        width = 0.95,
        height = 0.95,
      },
    },
  },
}
