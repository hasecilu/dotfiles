# Based on "Neovim Switcher" gist by Elijah Manor
# https://www.youtube.com/watch?v=LkHjJlSgKZY
#
# On Neovim 0.9.0 is introduced a new feature that allows to
# load a particular config firectory on "$XDG_CONF_HOME/$NVIM_APPNAME"
#
# Usage:
# - Add command below to $HOME/.zshrc file
# 	source $HOME/.config/nvim/switcher.sh
#
# - Clone all configs you want, ex:
# 	git clone https://github.com/nvim-lua/kickstart.nvim.git $HOME/.config/kickstart
# 	git clone https://github.com/LazyVim/starter $HOME/.config/LazyVim
# 	git clone --depth 1 https://github.com/AstroNvim/AstroNvim $HOME/.config/AstroNvim
#
# Other popular configurations: LunarVim, NvChad, nvim-config, SpaceVim, etc
#
# At the end you have 4 directories with different neovim stacks
# Still, if you want to use the default without using the switcher
# just use nvim as always.
#
# Also you can: alias v='nvim-lazy' to quickly open the LazyVim configuration.

alias nvim-lazy="NVIM_APPNAME=LazyVim nvim"
alias nvim-kick="NVIM_APPNAME=kickstart nvim"
alias nvim-astro="NVIM_APPNAME=AstroNvim nvim"

function nvims() {
	find -L "${XDG_CONFIG_HOME:-$HOME/.config}" -mindepth 2 -maxdepth 2 -name init.lua -o -name init.vim |
		awk -F/ '{print $(NF-1)}' |
		fzf --prompt '  Neovim config 󰄾 ' --layout=reverse --border --exit-0 |
		xargs -n1 bash -c 'NVIM_APPNAME="$1" exec nvim' --
}

bindkey -s ^a "nvims\n"
