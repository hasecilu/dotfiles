# Prusa Slicer

The firmware made by Miguel Risco Castillo ([repository][00]) has a very neat
feature to show a thumbnail of the object(s) before printing.

All information we need is on: [How to generate a gcode preview][01] Wiki page.

Essentially, we need to:
* Download the [Thumbnail Python script][02] that
embbeds the thumbnail in the gcode as comments:
* Copy the script to a know location like `$HOME/.config/PrusaSlicer/scripts/`
* Set a command for PrusaSlicer to use the script on
`Print Settings > Output options > Post-processing scripts` (`post_process`)
to "/usr/bin/python3 /home/uli/.config/PrusaSlicer/scripts/Thumbnail.py;"
* Set the thumbnail size and format on
`Printer Settings > General > Firmware` (`thumbnails`)
to "230x180/PNG".

[00]: https://github.com/mriscoc/Ender3V2S1
[01]: https://github.com/mriscoc/Ender3V2S1/wiki/How-to-generate-a-gcode-preview
[02]: https://raw.githubusercontent.com/mriscoc/Marlin_Ender3v2/Ender3v2-Released/slicer%20scripts/prusa-superslicer/professionalfirmware.py
