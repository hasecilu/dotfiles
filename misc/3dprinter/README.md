# My 3D printer

Ender 3 V2 from Creality

## Photo

![3D printer](../../images/3Dprinter.jpg)

## Hardware

* Creality 4.2.2 board
* 3D touch
* Filament detector
* Glass bed
* Yellow springs

## 3D printed parts upgrades

* Ender 3 Z axis knob
* Side Spool Mount
* PTFE cable clip
* 3D touch mount
* Side spool holder
* Z-motor spacer
* Tool holder

## Firmware

* Jyers firmware
* Default LCD graphics

