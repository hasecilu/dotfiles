# Marlin configuration files

Stock firmware is deficient, using a community maintained firmware enhances the
user experience.

I use the `Professional Firmware for the Creality Ender 3 V2` version made by
Miguel Risco Castillo. [Repository][00].

Also replaced the stock icons theme with [Giadej compilation][01].

The main changes are in these aspects:

- 5x5 UBL mesh
- 3D Touch
- Offset to nozzle: `{ -40, -14, -2.50 }`
- Filament sensor: `FIL_RUNOUT_STATE=HIGH`
- Bed size: `230x230`
- Change filament: `RUNOUT_DISTANCE`, `UNLOAD_LENGTH`
- Steps per mm: `{ 80.6, 80, 400, 98.6 }`

## Professional Firmware for the Creality Ender 3 V2/S1 Printers

Feature-rich Marlin fork that offers a lot of useful functionalities
for Ender 3 V2 3D printer.

The most useful feature is that you can create config files to make
replacements on the configuration files of the firmware instead of carrying
problematic `.diff` files.

### Preparation

- Clone the firmware & configuration repositories
- Create and activate a Python virtual environment
- Install the package `platformio`

```shell
git clone https://github.com/mriscoc/Ender3V2S1.git
git clone https://github.com/mriscoc/Special_Configurations.git
python3 -m venv ./Ender3V2S1/venv
source ./Ender3V2S1/venv/bin/activate
pip install -U platformio
```

### Make custom changes

There are two main options to generate the files with the changes:

#### Using the GUI

- On configuration directory create a new configuration file using the GUI
- Copy the generated files to firmware directory

```shell
cd Special_Configurations
python3 Configurator.pyw
cp MyConfiguration/*.h ../Ender3V2S1/Marlin/
cp MyConfiguration/platformio.ini ../Ender3V2S1/
```

#### Using a configuration file

We have the possibility to create a Python Script to create a custom build.

To achieve this we need a custom `.json` file to declare all custom tweaks
(`./_features/Extras.json`) and a Python script (`custom_build.py`) to
execute those replacements.

```shell
cd Special_Configurations
python3 custom_build.py
cp MyEnder3V2/*.h ../Ender3V2S1/Marlin/
cp MyEnder3V2/platformio.ini ../Ender3V2S1/
```

### Compile the firmware

Compile the firmware targeting the uC that the Creality 442 board uses.

```shell
cd ../Ender3V2S1/
platformio run -e STM32F103RE_creality
ls .pio/build/STM32F103RE_creality
```

## Updating firmware

1. Copy the `.bin` file to the microSD card
2. While the 3D printer is off plug the microSD
3. Turn on the 3D printer
4. Wait
5. After the update go to `Control`
6. Select `Reset to Defaults`
7. Check that the values are correct; steps per mm, bed size, temperature, etc.
8. Restart
9. Done!

## Leveling

1. Do a `Manual level`
2. Make a `Auto bed leveling`
3. Check the mesh
4. If the mesh has big noise, use `Convert the mesh to plane` to filter it.
5. Check the Z offset.

[00]: https://github.com/mriscoc/Ender3V2S1
[01]: https://github.com/mriscoc/Ender3V2S1/tree/Ender3V2S1-Released/display%20assets/Giadej%20compilation
