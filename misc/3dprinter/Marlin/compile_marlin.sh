#!/bin/bash

# Script to compile Marlin using a virtual enviroment

GREEN='\033[0;32m'
NC='\033[0m' # No Color

dir="$HOME/git/external/Ender3V2S1/"
e="STM32F103RE_creality"

cd $dir
source ./venv/venv/bin/activate
pip install -U platformio #Check for latest version
platformio run -e $e      # Generate binaries
deactivate

cd $dir/.pio/build/$e
echo -e "\n\n"
pwd | lolcat
echo
eza --icons --group-directories-first --no-quote

# https://stackoverflow.com/questions/36351627/bash-retrieve-the-last-modified-file-with-some-extension
last_bin="$(find . -type f -name \*.bin -printf "%T@ %p\0" | awk 'BEGIN {RS="\0";} {if (NR==1){minmtime=$1; $1=""; lastmodified=$0;} else if (minmtime<$1){minmtime=$1; $1=""; lastmodified=$0;}} END{print substr(lastmodified,4)}')"
#last_elf="$(find . -type f -name \*.elf -printf "%T@ %p\0" | awk 'BEGIN {RS="\0";} {if (NR==1){minmtime=$1; $1=""; lastmodified=$0;} else if (minmtime<$1){minmtime=$1; $1=""; lastmodified=$0;}} END{print substr(lastmodified,4)}')"

echo -e "\n\n"
while true; do
	read -p "Is the microSD connected? y/n: " yn
	case $yn in
	[Yy]*)
		mv -v /run/media/uli/E3V2/*.bin /run/media/uli/E3V2/old_bin
		cp -v ${last_bin} /run/media/uli/E3V2
		break
		;;
	[Nn]*) exit ;;
	*) echo "Please answer yes or no." ;;
	esac
done
