#!/usr/bin/python

import CreateConfigs

CreateConfigs.Generate(
    'MyEnder3V2',
    ['Ender3V2', '422', 'BLT', 'UBL', 'Extras']
)
